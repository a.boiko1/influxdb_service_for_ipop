from DatabasePool import DatabasePoolInstance
from influxdb import InfluxDBClient
from collections import defaultdict
import datetime
import re
import json

class InfluxCollector(DatabasePoolInstance):
    def __init__(self, config_dict):
        database_name = config_dict["influx"]["dbname"]
        
        self._db = \
            InfluxDBClient(
                config_dict["influx"]["host"],
                config_dict["influx"]["port"],
                database= database_name
            )
        self.flush_duration = config_dict['flush_duration']
        # check if database is exist
        if not{'name': database_name} \
                            in self._db.get_list_database():
             self._db.create_database(database_name)
    

    def _reset_data_held(self):
        self.data_held = {
            "Overlays": list(),
            "Nodes": list(),
            "Links": list(),
            "ChannelProperties" : list(),
            "IceProperties" :list(),
            "LogEntry": list(),
        }
        self._link_ids = defaultdict(set)

    def build_data(self, req, req_data, node_id ):   
           
            overlay_list = list()
            node_list = list()
            
            #print(json.dumps(req, indent=2 ))
            
            for ovrl_id in req_data:
                #self._logger.debug("Processing data for overlay_id"
                 #                  " {}".format(ovrl_id))
                if ovrl_id not in list(overlay['overlay_id'] for overlay in self.data_held["Overlays"]):
                    ovrl_init_data = {
                        "overlay_id": ovrl_id,
                        "num_nodes": 0,
                        "num_links": 0,
                        "name": "",
                        "description" :""
                    }
                    self.data_held["Overlays"].append(ovrl_init_data)
       
                     
                # Increment node counter in overlay if we did not have its data
                # for ovrl_id (meaning it is new in this overlay)
                # NOTE! This must be done before self.data_held["Nodes"] is
                # updated with node_data as it will add the key ovrl_id causing
                # this test to not behave as desired
                
                #find the overlay dict
                overlay_dicts = next((ovl_dict for ovl_dict in self.data_held['Overlays'] \
                                        if ovl_dict['overlay_id'] == ovrl_id))

                #find the list of node 
                node_dicts = list(node_dict for node_dict in self.data_held['Nodes'] \
                                        if node_dict['overlay_id'] == ovrl_id)
                
                if node_id not in list(id['node_id'] for id in node_dicts):
                     overlay_dicts.update({"num_nodes": int(overlay_dicts['num_nodes']) + 1})
                
                # TODO handle removal of a node within an interval

                #self._logger.debug(
                 #   "Updating node data for node_id {}".format(node_id))

                # Add the optional human-readable node name (if provided)
                node_data = dict()
                node_data['overlay_id'] = ovrl_id
                node_data['node_id'] = node_id
                node_data['geo_coordinate'] = ""
                if "NodeName" in req:
                    node_data["node_name"] = req["NodeName"]
                
                
                self.data_held["Nodes"].append(node_data)
             
                # Add/update data link data for the reporting node
                if "LinkManager" in req_data[ovrl_id] \
                        and req_data[ovrl_id]["LinkManager"]:
                    link_manager_data = req_data[ovrl_id]["LinkManager"]
                    
                    for link_id in link_manager_data[node_id]:
                        req_link_data = \
                            link_manager_data[node_id][link_id]
                        
                        if "ConnectionEdges" in req_data[ovrl_id]["Topology"]:
                            link_topo = req_data[ovrl_id]["Topology"]['ConnectionEdges'][link_id]
                        else:
                            link_topo = req_data[ovrl_id]["Topology"][link_id]
                        
                        #print(req_data[ovrl_id])

                        link_data = {
                             "overlay_id": ovrl_id ,
                             "link_id": link_id,
                             "node_id": req_link_data['NodeId'],
                             "peer_id": link_topo["PeerId"],
                             "created_time": link_topo["CreatedTime"],
                             "connected_state": link_topo["State"] ,
                             "connected_time": link_topo['ConnectedTime'],
                             "edge_type": link_topo["Type"],
                             "tap_name": req_link_data["TapName"],
                             "mac": req_link_data["MAC"],
                             "overlay_IP4": "" 
                        }
                        
                        # Increment link counter in overlay if we did not have
                        # its data for ovrl_id (meaning it is new in this
                        # overlay)
                        
                        if "NumEdges" in req_data[ovrl_id]['Topology']:
                            numEdges = int(req_data[ovrl_id]['Topology']['NumEdges'])
                            if numEdges != int(overlay_dicts['num_links']):
                                overlay_dicts.update({"num_links": numEdges })
                        else:
                            if link_id not in self._link_ids[ovrl_id]:
                                #print(link_id)
                                self._link_ids[ovrl_id].add(link_id)
                                overlay_dicts.update({"num_links": int(overlay_dicts['num_links']) + 1 })
                         
                       	#print(link_id) 
                        # Due to Stats field in req_link_data have a posibility to disappear.
                        # Need to check is there stats or not.
                        
                        try:
                            # Because of there are many Stats.
                            # To handdle that issue, need to check best_con == True.
                            link_stat = next(( stat_dict for stat_dict in req_link_data["Stats"] \
                                    if stat_dict["best_conn"] is True))

                            best_conn = link_stat["best_conn"]
                            total_byte_sent = link_stat["sent_total_bytes"]
                            byte_sent = link_stat["sent_bytes_second"]
                            byte_receive =  link_stat["recv_bytes_second"]
                            total_byte_receive = link_stat["recv_total_bytes"]
                            latency = link_stat["rtt"]
                            local_addr = link_stat["local_candidate"].split(":")[5]
                            remote_addr = link_stat["remote_candidate"].split(":")[5]

                        except KeyError:
                            best_conn = None
                            total_byte_sent = None
                            byte_sent = None
                            byte_receive =  None
                            total_byte_receive = None
                            latency = None
                            local_addr = None
                            remote_addr = None

                        channel_data = {
                                "link_id" : link_id,
                                "node_id" : node_id,
                                "byte_sent": byte_sent,
                                "total_byte_sent" : total_byte_sent,
                                "byte_receive" : byte_receive,
                                "total_byte_receive": total_byte_receive
                                }
                        ice_data = {
                                "link_id" : link_id,
                                "node_id" : node_id,
                                "role" :"",
                                "best_connection":  best_conn,
                                "local_addr": local_addr,
                                "remote_addr": remote_addr,
                                "latency": latency,
                                }

                        self.data_held["Links"].append(link_data)
                        self.data_held["ChannelProperties"].append(channel_data)
                        self.data_held["IceProperties"].append(ice_data)
                        #self.data_held["LogEntry"].append(log_entry_data)
                        
                #for mod_name in req_data[ovrl_id]:
                 #      if mod_name != "LinkManager":
                   #        if mod_name not in self.data_held:
                    #           print(req_data[ovrl_id][mod_name])
               
    def insert_data(self):
            time_stamp =  datetime.datetime.utcnow().replace(microsecond=0)
            points = list()
            
            
            self.add_points_dict('Overlays',['overlay_id'],points , time_stamp) 
            self.add_points_dict('Nodes',['node_id'], points, time_stamp)
            self.add_points_dict('Links',['node_id', 'link_id'],points, time_stamp)
            self.add_points_dict('ChannelProperties',['node_id','link_id'],points, time_stamp)
            self.add_points_dict('IceProperties',['node_id','link_id'],points, time_stamp)
            #self.add_points_dict('LogEntry','overlay_id',points, time_stamp)


            #insert data into the Influx
            self._db.write_points(points)

    # For restructure data format 
    def add_points_dict(self, key_name, tags, points, time_stamp):

        for data in self.data_held[key_name] :
                tagDict = dict()
                for tag in tags:
                  tagDict[tag] = data[tag]
                  del data[tag]

                points.append( {
                    "measurement":key_name,
                    "tags": tagDict,
                    "time": time_stamp,
                    "fields": data
                })

                
    # receive methods GET
    # /IPOP/overlays
    def get_overlays(self,timestamp):
        # self.flush_duration
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Overlays where time <= " + \
            str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" ORDER BY DESC LIMIT 1"
        print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Overlays'))
        sptime = re.split("T|\.|Z", listdb[0]["time"])
        time = sptime[0] + " " + sptime[1]
        json_data = {
            "current_state":
            {listdb[0]["overlay_id"]:
                {
                    "num_nodes": listdb[0]["num_nodes"],
                    "num_links": listdb[0]["num_links"],
            }
            },
            "intervalNo": time
        }
        return json_data
        
        
    # /IPOP/overlays/<overlayid>/nodes/<nodeid>
    def get_single_node(self, overlay_id, node_id, timestamp):
        dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Nodes where \"overlay_id\" = \'" + overlay_id + \
            "\' AND node_id = \'"+node_id+"\' AND time <= " + \
                str(timestamp)+" and time >= " + \
                str(timestamp-(self.flush_duration*10**9))+" ORDER BY DESC LIMIT 1"
        print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Nodes'))
        # print(db)
        data = {
            listdb[0]["node_id"]:
            {
                "node_name": listdb[0]["node_name"]
            }
        }
        return data
    
    def get_single_node_for_list_node(self, overlay_id, node_name, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Nodes where \"overlay_id\" = \'" + overlay_id + \
            "\' AND node_name = \'"+node_name+"\' AND time <= " + \
                str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" ORDER BY DESC LIMIT 1"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Nodes'))
        # print(listdb)
        timestm = re.split("T|\.|Z", listdb[0]["time"])
        time = timestm[0] + " " + timestm[1]
        # print(db)
        json_data = {
            overlay_id:
            {
                listdb[0]["node_id"]:
                {
                    "node_name": listdb[0]["node_name"]
                }
            },
            "intervalNo": time
        }  
        return json_data
    # /IPOP/overlays/<overlayid>/nodes
    def get_nodes(self, overlay_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        # query = "select last(node_name),node_name from Nodes where \"overlay_id\" = \'"+overlay_id+"\' GROUP BY \"node_id\""
        query = "select * from Nodes where time <= " + \
                str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" and \"overlay_id\" = \'" \
            + overlay_id +"\' GROUP BY \"node_id\" ORDER BY DESC LIMIT 1"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Nodes'))
        frame = '{\"'+overlay_id+'\":{"current_state":{}}}'
        json_data = json.loads(frame)
        # json_data.data["current_state"]

        for single_db in listdb:
            # print(single_db["node_name"])
            temp = self.get_single_node_for_list_node(
                overlay_id, single_db["node_name"], timestamp)
            # print(temp[overlay_id])
            json_data[overlay_id]["current_state"].update(temp[overlay_id])
        timestm = re.split("T|\.|Z", listdb[0]["time"])
        time = timestm[0] + " " + timestm[1]
        timejs = {"intervalNo": time}
        json_data.update(timejs)
        return json_data


    def get_iceProperties(self,link_id,node_id,timestamp):
        query = "select * from IceProperties where time <= " + str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" and \"link_id\"  = \'"+link_id + \
            "\' and \"node_id\" = \'"+node_id+"\' ORDER BY DESC LIMIT 1"
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='IceProperties'))
        data = {
            link_id:
            {
                node_id:
                {
                    "best_connection": listdb[0]["best_connection"],
                    "local_addr": listdb[0]["local_addr"],
                    "remote_addr": listdb[0]["remote_addr"],
                    "latency": listdb[0]["latency"]
                }
            }
        }
        return data

    def get_channelProperties(self,link_id, node_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from ChannelProperties where time <= " + \
            str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" and \"link_id\"  = \'"+link_id + \
            "\' and \"node_id\" = \'"+node_id+"\' ORDER BY DESC LIMIT 1"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='ChannelProperties'))
        ICP = self.get_iceProperties(listdb[0]["link_id"], listdb[0]["node_id"], timestamp)
        data = {
            link_id:
            {
                node_id:
                {
                    "byte_sent": listdb[0]["byte_sent"],
                    "total_byte_sent": listdb[0]["total_byte_sent"],
                    "byte_receive": listdb[0]["byte_receive"],
                    "total_byte_receive": listdb[0]["total_byte_receive"],
                    "IceProperties": ICP[listdb[0]["link_id"]][listdb[0]["node_id"]]
                }
            }
        }
        return data

    def get_single_link(self, overlay_id, node_id, peer_id, mac, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Links where time <= " + \
                str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" and \"overlay_id\"  = \'"+overlay_id + \
            "\' and \"node_id\" = \'"+node_id+"\'and \"peer_id\" = \'" + \
            peer_id+"\' and \"mac\" = \'"+mac+"\' ORDER BY DESC LIMIT 1"
        # print(query)
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Links'))
        # print(listdb)

        CP = self.get_channelProperties(
            listdb[0]["link_id"], listdb[0]["node_id"], timestamp)
        # print(db)
        data = {
            listdb[0]["node_id"]:
            {
                listdb[0]["link_id"]:
                {
                    "tap_name": listdb[0]["tap_name"],
                    "mac": listdb[0]["mac"],
                    "SrcNodeID": listdb[0]["node_id"],
                    "TgtNodeID": listdb[0]["peer_id"],
                    "peer_id": listdb[0]["peer_id"],
                    "EdgeID": listdb[0]["link_id"],
                    "tap_name": listdb[0]["tap_name"],
                    "connected_time": listdb[0]["connected_time"],
                    "created_time": listdb[0]["created_time"],
                    "edge_type": listdb[0]["edge_type"],
                    "ChannelProperties": CP[listdb[0]["link_id"]][listdb[0]["node_id"]]
                }
            }
        }
        return data
    # /IPOP/overlays/<overlayid>/nodes/<nodeid>/links
    def get_links_for_a_node(self, overlay_id, node_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Links where time <= " + \
                str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" and \"overlay_id\"  = \'"+overlay_id + \
            "\' and \"node_id\" = \'"+node_id+"\' GROUP BY link_id ORDER BY DESC LIMIT 1"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Links'))
        frame = '{\"'+overlay_id+'\":{\"'+node_id+'\":{"current_state":{}}}}'
        json_data = json.loads(frame)

        for single_db in listdb:
            temp = self.get_single_link(
                overlay_id, single_db["node_id"], single_db["peer_id"], single_db["mac"], timestamp)
            # print(temp)
            json_data[overlay_id][node_id]["current_state"].update(temp[node_id])
        timestm = re.split("T|\.|Z", listdb[0]["time"])
        time = timestm[0] + " " + timestm[1]
        timejs = {"intervalNo": time}
        json_data.update(timejs)
        return json_data
    # /IPOP/overlays/<overlayid>/links
    def get_links_in_an_overlay(self, overlay_id, timestamp):
        # self.flush_duration
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Links where time <= " + \
                str(timestamp)+" and time >= " + \
            str(timestamp-(self.flush_duration*10**9))+" and \"overlay_id\"  = \'"+overlay_id + \
            "\'GROUP BY link_id ORDER BY DESC LIMIT 2"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Links'))
        frame = '{\"'+overlay_id+'\":{"current_state":{}}}'
        json_data = json.loads(frame)

        for single_db in listdb:
            temp = self.get_links_for_a_node(overlay_id, single_db["node_id"], timestamp)
            # print(temp)
            frame_node = {single_db["node_id"] :{}}
            json_data[overlay_id]["current_state"].update(frame_node)
            json_data[overlay_id]["current_state"][single_db["node_id"]].update(temp[overlay_id][single_db["node_id"]]["current_state"])
            
        timestm = re.split("T|\.|Z", listdb[0]["time"])
        time = timestm[0] + " " + timestm[1]
        timejs = {"intervalNo": time}
        json_data.update(timejs)
        return json_data

